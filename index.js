
let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: [
        "Pikachu", "Charizard", "Squirtle", "Bulbasaur" 
        ],
    friends: {
        hoenn:[
            "May", "Max"
            ],
        kanto: [
            "Brock", "Misty"
            ]
    },
    talk: function(){
        console.log("Pikachu! I choose you")
    }
}

console.log(trainer)
trainer.talk()

// Using dot notation
console.log('Result from dot notation: ');
console.log(trainer.name)
// Using the square bracket notation
console.log('Result from square bracket notation: ');
console.log(trainer['pokemon'])


function Pokemon(name, level, health, attack){

    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;
    this.tackle = function(poke){
        console.log(this.name + " tackled " + poke.name)
        poke.health = poke.health - this.attack
        console.log(poke.name + "'s health is now reduced to " + poke.health)
    }
    this.faint = function(){
        if(this.health <= 0)
        console.log(this.name + " fainted")
    }
}

let pokemon1 = new Pokemon('Pikachu', 12)
console.log(pokemon1)

let pokemon2 = new Pokemon('Geodude', 8)
console.log(pokemon2)

let pokemon3 = new Pokemon('Mewtwo', 100)
console.log(pokemon3)

pokemon2.tackle(pokemon1)
pokemon1.faint()
console.log(pokemon1)

pokemon3.tackle(pokemon2)
pokemon2.faint()
console.log(pokemon2)
/*Pokemon.tackle = function(){
    console.log(pokemon2.name + " tackled " + pokemon1.name)
    pokemon1.health = pokemon1.health - pokemon2.attack
    console.log(pokemon1.name + "'s health is now reduced to " + pokemon1.health)
}*/

/*console.log(pokemon1)

Pokemon.tackle = function(){
    console.log(pokemon3.name + " tackled " + pokemon2.name)
    pokemon2.health = pokemon2.health - pokemon3.attack
    console.log(pokemon2.name + "'s health is now reduced to " + pokemon2.health)
}

Pokemon.tackle()

Pokemon.faint = function(){
    if(pokemon1.health <= 0)
        console.log(pokemon1.name + " fainted")
    else if(pokemon2.health <= 0)
        console.log(pokemon2.name + " fainted")
    else if(pokemon3.health <= 0)
        console.log(pokemon3.name + " fainted")
}

Pokemon.faint()


console.log(pokemon2)*/